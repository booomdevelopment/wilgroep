<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

function ajax_filter_cats() {/*
    if ($_POST['locatie'] == "") {
        $args_c = array(
            'post_type' => 'vacatures',
            'paged' => $paged,
            'posts_per_page' => 5,
            'ignore_sticky_posts' => 1
        );
    }
    else {   
        $args_c = array(
            'post_type' => 'vacatures',
            'tax_query' => array(
                array(
                    'taxonomy' => 'locatie',
                    'field' => 'slug',
                    'terms' => $_POST['locatie'],
                )
            )
        );
    }*/
if (!empty($_POST['locatie'])) {
    $locatie_arr = array (
        'taxonomy' => 'locatie',
        'field'    => 'slug',
        'terms'    => $_POST['locatie'],
    );
}

if (!empty($_POST['uurverband'])) {
    $uurverband_arr = array (
        'taxonomy' => 'uurverband',
        'field'    => 'slug',
        'terms'    => $_POST['uurverband'],
    );
}
    
if (!empty($_POST['beroepsgroep'])) {
    $beroepsgroep_arr = array (
        'taxonomy' => 'beroepsgroep',
        'field'    => 'slug',
        'terms'    => $_POST['beroepsgroep'],
    );
}

if (!empty($_POST['s'])) {          
    $arr = array(
        'post_type'  => 'vacatures',
        's'      =>  $_POST['s'], 
        'engine' => 'default', 
        'tax_query' => array( // tax_query support
            $locatie_arr,
            $uurverband_arr,
            $beroepsgroep_arr
        ), 
    );
}

$arr = array(
    'post_type'  => 'vacatures',
    'engine' => 'default', 
    'ignore_sticky_posts' => 1,
    'tax_query' => array( // tax_query support
        $locatie_arr,
        $uurverband_arr,
        $beroepsgroep_arr
        ) 
);
    $query_n = new \WP_Query( $arr );

    if( $query_n->have_posts() ) :
        while( $query_n->have_posts() ): $query_n->the_post();
        $get_id = get_the_ID();
        $get_title = get_field('vacature_titel');
        $get_map = get_field('vacature_locatie');
        $get_tijd = get_field('vacature_tijd');
        $get_school = get_field('vacature_opleiding');
        $get_link = get_the_permalink();
        
        echo <<< html
            <div id="${get_id}" class="vacature-item">
                <p class="v-title">${get_title}</p>
                <div class="v-info">
                    <div><img src="/app/uploads/2020/04/map.svg" alt="map icon"><p>${get_map}</p></div>
                    <div><img src="/app/uploads/2020/04/clock.svg" alt="clock icon"><p>${get_tijd}</p></div>
                    <div><img src="/app/uploads/2020/04/school.svg" alt="school icon"><p>${get_school}</p></div>
                </div>
                <ul class="v-usp">
html;
                    if( have_rows('vacature_usp') ):
                        while ( have_rows('vacature_usp') ) : the_row();
                                echo '<li>';
                                the_sub_field('vacature_usp_item');
                                echo '</li>';
                        endwhile;
                    endif;
                echo <<< html
                </ul>
                <a class="main-button-vac" href="${get_link}">Bekijk vacature</a>
            </div>
html;


        endwhile;
    wp_reset_postdata();
    endif;
die();
}


add_action('wp_ajax_catfilter', __NAMESPACE__ . '\\ajax_filter_cats');
add_action('wp_ajax_nopriv_catfilter', __NAMESPACE__ . '\\ajax_filter_cats');