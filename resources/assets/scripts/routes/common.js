import Swiper, { Autoplay, Navigation } from 'swiper'; 

export default {
  init() {
    // JavaScript to be fired on all pages
      var hamburger = $('#hamburger-icon');
      var hamburgerhelper = $('.nav-menu');
      hamburgerhelper.click(function() {
        hamburger.toggleClass('active');
        $('body').toggleClass('mobile-menu-active');
        $('.mobile-menu').fadeToggle();
        //return false;
      });

      // Ajax cat filter
      $('.filter__a').on('change', function () {
          var filter = $('#filter_c');
          $.ajax({
              url: filter.attr('action'),
              data: filter.serialize(),
              type: filter.attr('method'),
              beforeSend: function () {
                  $('#aj_resp').fadeTo('normal', 0.4, function () {

                  });
              },
              success: function (data) {
                  $('#aj_resp').html(data);
                  $('#aj_resp').fadeTo('normal', 1, function () {

                  });
              },
          });
          return false;
      });

      //logo slider

      const swiperContainerLogo = document.getElementById('swiper-container-logo');

      if (swiperContainerLogo) {
  
        Swiper.use([Autoplay, Navigation]);
  
        const swiperContainerLogoSwiper = new Swiper('.swiper-container-logo', {
          // Optional parameters
          slidesPerView: 5,
          spaceBetween: 35,
          loop: true,
          autoplay: {
            delay: 2000,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
            319: {
              slidesPerView: 2,
              spaceBetweenSlides: 15,
            },
            450: {
              slidesPerView: 3,
              spaceBetweenSlides: 15,
            },
            1000: {
              slidesPerView: 5,
              spaceBetweenSlides: 15,
            },
          },
        })
        swiperContainerLogoSwiper.init();
      }

      //hero slider

      const swiperContainerHero = document.getElementById('swiper-container-hero');

      if (swiperContainerHero) {
  
        Swiper.use([Autoplay, Navigation]);
  
        const swiperContainerHeroSwiper = new Swiper('.swiper-container-hero', {
          // Optional parameters
          slidesPerView: 1,
          loop: true,
          autoplay: {
            delay: 5000,
          },
          navigation: {
            nextEl: '.swiper-button-next-hero',
            prevEl: '.swiper-button-prev-hero',
          },
        })
        swiperContainerHeroSwiper.init();
      }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
