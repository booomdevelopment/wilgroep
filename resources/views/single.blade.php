@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-reverse-header')
    @include('partials.content-text-photo-post')
    @include('partials.content-cta-banner')
  @endwhile
@endsection
