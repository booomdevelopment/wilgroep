{{--
  Template Name: Baan template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <section class="alt-header">
      <div class="width-inner alt-header-wrapper">
        <div class="text-image-header flex-it f-row f-just-between">
          <div class="text-left">
            <div class="text-content flex-it f-col">
              @group('inhoud_links')
                <h1>@sub('linker_titel')</h1>
                <p>@sub('linker_tekst')</p>
                <a href="@sub('linker_knop_link')">@sub('linker_knop')</a>
              @endgroup
            </div>
          </div>
          <div class="image-right">
            <img src="@field('foto_rechts', 'url')" alt="@field('foto_rechts', 'alt')" />
          </div>
        </div>
      </div>
    </section>
    @include('partials.content-text-button-image')
    @include('partials.content-text-double')
    @include('partials.content-baan-form') 
    @include('partials.content-text-image')
    @include('partials.content-vacature-list') 
    @include('partials.content-ups-banner')
    @include('partials.content-cta-banner')
  @endwhile
@endsection
