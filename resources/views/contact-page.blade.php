{{--
  Template Name: Contact template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-small-header')
    @include('partials.content-text-image-contact')
    @include('partials.content-contact-form') 
    @include('partials.content-cta-banner')
    @include('partials.content-text-list')
  @endwhile
@endsection
