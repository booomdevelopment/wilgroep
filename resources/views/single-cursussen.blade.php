@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-reverse-header')
    @include('partials.content-text-list-cursussen')
    <section class="baan-form-container"  style="margin-bottom: 100px;">
      <div class="width-inner baan-form-wrapper">
          <div class="form-information">
            <h2 class="h2-title">@field('formulier_titel')</h2>
            <p>@field('formulier_text')</p>
          </div>
          <div class="form-wrapper cursus-form-wrapper">
              <?php echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true"]' ); ?>
          </div>
      </div>
  </section>
  @endwhile
@endsection
