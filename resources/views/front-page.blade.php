@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <section class="home-header">
      <div class="home-header-wrapper">
        <div class="image-container">
          {{-- <img src="@field('home_header_afbeelding', 'url')" alt="@field('home_header_afbeelding', 'url')"> --}}
          <!-- Slider main container -->
          <div class="swiper-container-hero" id="swiper-container-hero">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                @fields('home_header_afbeeldingen')
                  <div class="swiper-slide"><img src="@sub('home_header_afbeelding', 'url')" alt="@sub('home_header_afbeelding', 'url')"></div>
                @endfields
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev-hero"><p><</p></div>
            <div class="swiper-button-next-hero"><p>></p></div>
          </div>

        </div>
        <div class="overlap-blocks flex-it f-row f-just-between">
          <div class="overlap-block overlap-block-1">
            <div class="block-content flex-it f-col">
              @group('linker_blok')
                <h2>@sub('linker_titel')</h2>
                <p>@sub('linker_tekst')</p>
                <a href="@sub('linker_knop_link')">@sub('linker_knop')</a>
              @endgroup
            </div>
          </div>
          <div class="overlap-block overlap-block-2">
            <div class="block-content flex-it f-col">
              @group('rechter_blok')
                <h2>@sub('rechter_titel')</h2>
                <p>@sub('rechter_tekst')</p>
                <a href="@sub('rechter_knop_link')">@sub('rechter_knop')</a>
              @endgroup
            </div>
          </div>
        </div>
      </div>
    </section>
    @include('partials.content-text-list-home')
    @include('partials.content-text-double')
    @include('partials.content-cursus-loop')
    @include('partials.content-ups-banner')
    @include('partials.content-cta-banner')
    @include('partials.content-text-button-image')
    @include('partials.content-vacature-list') 
    @include('partials.content-logo-slider') 
  @endwhile
@endsection
