{{--
  Template Name: Landing page template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-reverse-header')
    <section class="content-row-landing main-padding">
      @hasfield('foto_banner_afbeelding')
        <div class="landing-page-banner width-inner">
              <img src="@field('foto_banner_afbeelding', 'url')" alt="@field('foto_banner_afbeelding', 'alt')" />
        </div>
      @endfield
      <div class="landing-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
        <div class="landing-text">
            <h1>@field('landing_page_titel')</h1>
            <div>@field('landing_page_editor')</div>    
        </div>
        <div class="double-border">
            <span class="border-1"></span>
            <span class="border-2"><span>
        </div>
      </div>
    </section>
    @include('partials.content-cta-banner')
  @endwhile
@endsection



