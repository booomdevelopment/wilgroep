{{--
  Template Name: Vacature template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-small-header')
    @include('partials.content-vacature-archive')
    @include('partials.content-cta-banner')
  @endwhile
@endsection
