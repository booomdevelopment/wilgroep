{{--
  Template Name: Posts blog template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-small-header') 
    <section class="posts-loop-container main-padding">
      <div class="posts-loop-intro">
        <div class="posts-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
          <div class="posts-text">
            @group('blog_tekst_blok')
              <h4 class="h4-quote">@sub('blog_quote')</h4>
              <h2 class="h2-title">@sub('blog_titel')</h2>
              <div>@sub('blog_tekst')</div>    
            @endgroup
          </div>
          <div class="posts-foto">
              <img src="@field('blog_foto_blok', 'url')" alt="@field('blog_foto_blok', 'url')" />
          </div>
          <div class="double-border">
              <span class="border-1"></span>
              <span class="border-2"><span>
          </div>
        </div>
      </div>
      <div class="posts-loop-wrapper width-inner">
        <?php
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        ?>
        @php
        $query = new WP_Query([
          'post_type' => 'post',
          'category_name' => 'blog',
          'paged' => $paged,
          'posts_per_page' => 5,
          'ignore_sticky_posts' => 1
        ]);
        @endphp
        @posts
        <div class="single-post-item flex-it f-row f-just-between f-align-center">
          <div class="single-post-content">
            <h4 class="h4-quote">@field('bericht_subtitel')</h4>
            <h2 class="h2-title">@field('bericht_titel')</h2>
            <div>@field('bericht_samenvatting')</div>   
            <a href="@permalink" class="main-button">Lees meer</a> 
          </div>
          <div class="single-post-photo">
            <img src="@field('bericht_afbeelding', 'url')" alt="@field('bericht_afbeelding', 'alt')" />
          </div>
        </div>
        @endposts
        <div class="post-loop-pagination">
          <?php
            $big = 999999999; // need an unlikely integer
            
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $query->max_num_pages
            ) );
          ?>
        </div>
      </div>
    </section>
    @include('partials.content-cta-banner')
    @include('partials.content-text-list')
  @endwhile
@endsection
