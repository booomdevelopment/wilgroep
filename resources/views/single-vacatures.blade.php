@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-reverse-header')
    @include('partials.content-text-list-vacature')
    <section class="baan-form-container">
      <div class="width-inner baan-form-wrapper">
          <div class="form-information">
              <h2 class="h2-title">@field('formulier_titel')</h2>
              <p>@field('formulier_tekst')</p>
          </div>
          <div class="form-wrapper">
              <?php echo do_shortcode( '[contact-form-7 id="492" title="Sollicitatie formulier"]' ); ?>
          </div>
      </div>
  </section>
    @include('partials.content-cta-banner')
  @endwhile
@endsection
