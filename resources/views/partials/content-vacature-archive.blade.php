<section class="vacature-archive-container main-padding">
    <div class="width-inner flex-it f-row f-just-between">
        <div class="vacatures-filter">
            <h3>Filters</h3>
            <div class="select-container">
                <div class="select-family-wrapper">
                    <?php
                        $termchildren1 = get_terms('locatie');
                        $termchildren2 = get_terms('uurverband');
                        $termchildren3 = get_terms('beroepsgroep');
                    ?>
                    <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter_c">
                        <fieldset>
                            <p>Locatie's</p>
                            @foreach ($termchildren1 as $tc)
                                <span class="input-wrapper">
                                <label>
                                    <input class="filter__a" id="{{ $tc->slug }}" type="checkbox" name="locatie[]" value="{{ $tc->slug }}" />
                                    <span class="checkmark"></span>
                                    {{ $tc->name }}
                                </label>
                                </span>
                            @endforeach
                        </fieldset>
                        <fieldset>
                            <p>Uurverbanden</p>
                            @foreach ($termchildren2 as $tc)
                                <span class="input-wrapper">
                                    <label>
                                        <input class="filter__a" id="{{ $tc->slug }}" type="checkbox" name="uurverband[]" value="{{ $tc->slug }}" />
                                        <span class="checkmark"></span>
                                        {{ $tc->name }}
                                    </label>
                                </span>
                            @endforeach
                        </fieldset>
                        <fieldset>
                            <p>Beroepsgroepen</p>
                            @foreach ($termchildren3 as $tc)
                                <span class="input-wrapper">
                                    <label>
                                        <input class="filter__a" id="{{ $tc->slug }}" type="checkbox" name="beroepsgroep[]" value="{{ $tc->slug }}" />
                                        <span class="checkmark"></span>
                                        {{ $tc->name }}
                                    </label>
                                </span>
                            @endforeach
                        </fieldset>
                        <input type="hidden" name="action" value="catfilter">
                    </form>
                </div>
            </div>
        </div>
        <div class="vacatures-loop">
            <h2 class="h2-title">@field('vacatures_titel')</h2>
            <div id="aj_resp" class="vacatures">
                @php
                    $query = new WP_Query([
                    'post_type' => 'vacatures',
                    'ignore_sticky_posts' => 1
                    ]);
                @endphp
                @posts
                    <div id="{{ get_the_ID() }}" class="vacature-item">
                        <p class="v-title">@field('vacature_titel')</p>
                        <div class="v-info">
                            <div><img src="/app/uploads/2020/04/map.svg" alt="map icon"><p>@field('vacature_locatie')</p></div>
                            <div><img src="/app/uploads/2020/04/clock.svg" alt="clock icon"><p>@field('vacature_tijd')</p></div>
                            <div><img src="/app/uploads/2020/04/school.svg" alt="school icon"><p>@field('vacature_opleiding')</p></div>
                        </div>
                        <ul class="v-usp">
                            @php
                                $fieldcounter = 1;
                            @endphp
                            @fields('vacature_usp')
                                <li>@sub('vacature_usp_item')</li>
                                @php
                                    $fieldcounter++;
                                    if ($fieldcounter > 3) {
                                        break;
                                    };
                                @endphp
                            @endfields
                        </ul>
                        <a class="main-button-vac" href="@permalink">Bekijk vacature</a>
                    </div>
                @endposts
            </div>
        </div>
    </div>
</section>