<header class="banner">
  <div class="width-inner">
    <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/wil-logo-nieuw.png')" class="menu-logo" alt="logo" /> </a>
    <nav class="nav-primary flex-it f-row f-just-between f-align-center">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
      <div class="nav-select">
        <div class="nav-phone">
          <a href="tel:0646372525">
            <img src="@asset("images/phone-svg.svg")" width="1em" height="1em"/>
            <p>0646372525</p>
          </a>      
        </div>
        <div class="nav-spacer-wrapper">
          <span class="nav-spacer"></span>
        </div>
        <div class="nav-menu">
          <p>MENU</p>
          <div id="hamburger-icon" title="Menu">
            <span class="line line-1"></span>
            <span class="line line-2"></span>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>
@include('partials.content-mobile-menu')
