<section class="small-header">
    <div class="small-header-wrapper width-inner flex-it f-row f-just-between f-align-center f-wrap">
        <h1>@field('header_titel')</h1>
        <div class="small-header-button-wrapper">
            <a class="main-button" href="/nieuws/">Bekijk nieuws</a>
            <a class="main-button" href="/blog/">Bekijk blog</a>
        </div>
    </div>
</section>