<section class="double-text-row main-padding">
    <div class="width-inner flex-it f-row f-just-between f-align-center">
        <div class="text-row-wrapper">
            @field('tekst_veld_links')
        </div>
        <div class="text-row-wrapper">
            @field('tekst_veld_rechts')  
        </div>
    </div>
</section>
<section class="double-text-row main-padding">
    <div class="width-inner flex-it f-row f-just-between f-align-center">
        <div class="text-row-wrapper">
            @field('tekst_veld_links_kopie')
        </div>
        <div class="text-row-wrapper">
            @field('tekst_veld_rechts_kopie')  
        </div>
    </div>
</section>