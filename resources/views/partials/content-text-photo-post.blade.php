<section class="content-row-bericht main-padding">
    <div class="bericht-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
        <div class="bericht-text">
            <h4 class="h4-quote">@field('bericht_subtitel')</h4>
            <h1>@field('bericht_titel')</h1>
            <div>@field('bericht_editor')</div>    
        </div>
        <div class="bericht-foto">
            <img src="@field('bericht_afbeelding', 'url')" alt="@field('bericht_afbeelding', 'url')" />
        </div>
        <div class="double-border">
            <span class="border-1"></span>
            <span class="border-2"><span>
        </div>
    </div>
</section>

