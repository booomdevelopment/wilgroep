<section style="background-image:url('@field("cta_foto", "url")')" class="cta-banner">
    <div class="cta-wrapper width-inner">
        <h2>@field('cta_titel')</h2>
        <a href="@field('cta_knop_link')" class="main-button">@field('cta_knop_tekst')</a>
    </div>
</section>