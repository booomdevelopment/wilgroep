<footer class="content-info">
    <section class="footer-top">   
        <div class="width-inner">
            <div class="top-row-container flex-it f-row f-just-between f-align-center">
                <div class="top-row top-row-1">
                    <h3>Pagina's</h3>
                    <ul class="footer-list">
                        @options('pagina_repeater')
                            <li><a href="@sub('pagina_link')">@sub('pagina_naam')</a></li>
                        @endoptions
                    </ul>
                </div>
                <div class="top-row top-row-2">
                    <h3>Nieuws</h3>
                    <div class="footer-nieuws">
                        @query([
                            'post_type' => 'post',
                            'orderby' => 'date',
                            'order'   => 'DESC',
                            'posts_per_page' => 2
                        ])
                        @posts
                            <div class='footer-nieuws-item'>
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>
                        @endposts
                    </div>
                </div>
                <div class="top-row top-row-3">
                    <h3>Contact</h3>
                    <div class="contact-list">
                        @options('contact_informatie')
                            <div class="contact-item"><img src="@asset('images/phone.svg')" alt="Telefoon icon" /><a href="tel:@sub('telefoon_nummer')">@sub('telefoon_nummer')</a></div>
                            <div class="contact-item"><img src="@asset('images/mail.svg')" alt="Mail icon" /><a href="mailto:@sub('email_adres')">@sub('email_adres')</a></div>
                            <div class="contact-item"><img src="@asset('images/map.svg')" alt="Map icon" /><a href="@sub('adres_maps_link')">@sub('adres_tekst')</a></div>
                        @endoptions
                    </div>
                </div>
            </div>
            <div class="bottom-row flex-it f-row f-just-between f-align-center">
                <div class="footer-cta">
                    <a class="main-button" href="/leerwerktraject/">Ik zoek een baan</a>
                    <a class="main-button-alt" href="/zoek-personeel/">Ik zoek personeel</a>
                    <a href="https://www.normeringarbeid.nl/"><img style="width: 60px;" src="@asset('images/normering.png')" alt=""></a>
                </div>
                <div class="footer-socials flex-it f-row f-just-between f-align-center">
                    @options('social_media_knoppen')
                        <a href="@sub('facebook_link')"><img src="@asset('images/facebook.svg')" alt="Facebook logo" /></a>
                        <a href="@sub('instagram_link')"><img src="@asset('images/instagram.svg')" alt="Instagram logo" /></a>
                        <a href="@sub('linkedin_link')"><img src="@asset('images/linkedin.svg')" alt="LinkedIn logo" /></a>
                    @endoptions
                </div>
            </div>
        </div>
    </section>
    <section class="footer-bottom">
        <div class="width-inner flex-it f-row f-just-between f-align-center">
            <div class="bottom-row">
                @options('footer_algemene_informatie')
                    <a style="margin-right: 30px;" href="@sub('privacy_verklaring')">Privacy verklaring</a>
                    <a href="@sub('algemene_voorwaarden')">Algemene voorwaarden</a>
                @endoptions
            </div>
            <div style="margin-left: -30px;" class="bottom-row">
                <p>Copyright © WIL Groep <?php echo date("Y"); ?></p>
            </div>
            <div class="bottom-row">
                <a href="https://booomdigital.nl">Website door: <span class="b__span">BOOOM Digital</span></a>
            </div>
        </div>
    </section>
</footer>
