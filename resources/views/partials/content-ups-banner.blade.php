<section class="ups-banner">
    <div class="ups-wrapper width-inner">
        <ul class="list flex-it f-row f-just-between f-align-center">
            @fields('usp_items')
                <li>@sub('usp_tekst')</li>
            @endfields
        </ul>
    </div>
</section>