<section class="about-row-image-button main-padding">
    <div class="about-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
    <div class="about-text">
        @group('links_tekst_knop_blok')
            <h4 class="h4-quote">@sub('links_quote')</h4>
            <h2 class="h2-title">@sub('links_titel')</h2>
            <p>@sub('links_tekst')</p>
            <a class="main-button" href="@sub('links_knop_link')">@sub('links_knop')</a>
        @endgroup
    </div>
    <div class="about-image">
        <img src="@field('rechts_foto_knop_blok', 'url')" alt="@field('rechts_foto_knop_blok', 'url')" />
    </div>
    <div class="double-border">
        <span class="border-1"></span>
        <span class="border-2"><span>
    </div>
    </div>
</section>