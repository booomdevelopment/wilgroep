<section class="small-header">
    <div class="small-header-wrapper width-inner flex-it f-row f-just-between f-align-center f-wrap">
        @if (is_archive('cursussen'))
            <h1>Leerwerktrajecten</h1>
            <a class="main-button" href="/contact">Neem contact op</a>
        @else
            <h1>@field('header_titel')</h1>
            <a class="main-button" href="@field('header_knop_link')">@field('header_knop_tekst')</a>
        @endif
    </div>
</section>