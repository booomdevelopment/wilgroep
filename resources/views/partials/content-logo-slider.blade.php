<section class="c__logo-slider">
    <div class="w__logo-slider width-inner">
        <!-- Slider main container -->
        <div class="swiper-container-logo" id="swiper-container-logo">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                @fields('logo_slider')
                <div class="swiper-slide">
                    <img src="@sub('logo_image', 'url')" alt="">
                </div>
                @endfields

            </div>
        </div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>