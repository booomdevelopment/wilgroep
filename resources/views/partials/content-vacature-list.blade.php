<section class="vacature-container main-padding">
    <div class="width-inner">
    <h2 class="h2-title">@field('vacatures_titel')</h2>
    <div class="vacatures">
        <div class="vacature-item">
        <p class="v-title">Fulltime Functiebeschrijving Arnhem</p>
        <div class="v-info">
            <div><img src="@asset('images/map.svg')" alt="map icon"><p>Arnhem</p></div>
            <div><img src="@asset('images/clock.svg')" alt="clock icon"><p>32 tot 40 uur</p></div>
            <div><img src="@asset('images/school.svg')" alt="school icon"><p>MBO</p></div>
        </div>
        <ul class="v-usp">
            <li>USP over vacature</li>
            <li>USP over vacature</li>
            <li>USP over vacature</li>
        </ul>
        <a class="main-button-vac" href="#">Bekijk vacature</a>
        </div>
        <div class="vacature-item">
        <p class="v-title">Fulltime Functiebeschrijving Arnhem</p>
        <div class="v-info">
            <div><img src="@asset('images/map.svg')" alt="map icon"><p>Arnhem</p></div>
            <div><img src="@asset('images/clock.svg')" alt="clock icon"><p>32 tot 40 uur</p></div>
            <div><img src="@asset('images/school.svg')" alt="school icon"><p>MBO</p></div>
        </div>
        <ul class="v-usp">
            <li>USP over vacature</li>
            <li>USP over vacature</li>
            <li>USP over vacature</li>
        </ul>
        <a class="main-button-vac" href="#">Bekijk vacature</a>
        </div>
        <div class="vacature-item">
        <p class="v-title">Fulltime Functiebeschrijving Arnhem</p>
        <div class="v-info">
            <div><img src="@asset('images/map.svg')" alt="map icon"><p>Arnhem</p></div>
            <div><img src="@asset('images/clock.svg')" alt="clock icon"><p>32 tot 40 uur</p></div>
            <div><img src="@asset('images/school.svg')" alt="school icon"><p>MBO</p></div>
        </div>
        <ul class="v-usp">
            <li>USP over vacature</li>
            <li>USP over vacature</li>
            <li>USP over vacature</li>
        </ul>
        <a class="main-button-vac" href="#">Bekijk vacature</a>
        </div>
    </div>
    <div class="more-vac-button">
        <a class="main-button" href="@field('vacatures_knop_link')">@field('vacatures_knop_tekst')</a>
    </div>
    </div>
</section>