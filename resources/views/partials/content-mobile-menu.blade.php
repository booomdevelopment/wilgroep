<header class="mobile-menu">
    <section class="menu-container">
        <div class="width-inner">
            <div class="top-row-container flex-it f-row f-just-between f-align-center">
                <div class="top-row top-row-1">
                    <h3>Pagina's</h3>
                    <ul class="mobile-m-list">
                        @options('pagina_repeater')
                            <li><a href="@sub('pagina_link')">@sub('pagina_naam')</a></li>
                        @endoptions
                    </ul>
                </div>
                <div class="top-row top-row-2">
                    <h3>Nieuws</h3>
                    <div class="mobile-m-nieuws">
                        @query([
                            'post_type' => 'post',
                            'orderby' => 'date',
                            'order'   => 'DESC',
                            'posts_per_page' => 2
                        ])
                        @posts
                            <div class='mobile-m-nieuws-item'>
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>
                        @endposts
                    </div>
                </div>
                <div class="top-row top-row-3">
                    <h3>Contact</h3>
                    <div class="contact-list">
                        @options('contact_informatie')
                            <div class="contact-item"><img src="@asset('images/telefoon-wit.svg')" alt="Telefoon icon" /><a href="tel:@sub('telefoon_nummer')">@sub('telefoon_nummer')</a></div>
                            <div class="contact-item"><img src="@asset('images/mail-wit.svg')" alt="Mail icon" /><a href="mailto:@sub('email_adres')">@sub('email_adres')</a></div>
                            <div class="contact-item"><img src="@asset('images/locatie-wit.svg')" alt="Map icon" /><a href="@sub('adres_maps_link')">@sub('adres_tekst')</a></div>
                        @endoptions
                    </div>
                </div>
            </div>
            <div class="bottom-row flex-it f-row f-just-between f-align-center">
                <div class="mobile-m-cta">
                    <a class="main-button" href="/ik-zoek-een-baan/">Ik zoek een baan</a>
                    <a class="main-button-alt" href="/ik-zoek-personeel/">Ik zoek personeel</a>
                </div>
                <div class="mobile-m-socials flex-it f-row f-just-between f-align-center">
                    @options('social_media_knoppen')
                        <a href="@sub('facebook_link')"><img src="@asset('images/facebook-wit.svg')" alt="Facebook logo" /></a>
                        <a href="@sub('instagram_link')"><img src="@asset('images/instagram-wit.svg')" alt="Instagram logo" /></a>
                        <a href="@sub('linkedin_link')"><img src="@asset('images/linkedin-wit.svg')" alt="LinkedIn logo" /></a>
                    @endoptions
                </div>
            </div>
        </div>
    </section>
</header>
