<section id="personeelform" class="baan-form-container">
    <div class="width-inner baan-form-wrapper">
        <div class="form-information">
            <h2 class="h2-title">@field('formulier_titel')</h2>
            <p>@field('formulier_tekst')</p>
        </div>
        <div class="form-wrapper">
            <?php echo do_shortcode( '[contact-form-7 id="244" title="Personeel formulier"]' ); ?>
        </div>
    </div>
</section>