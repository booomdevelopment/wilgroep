<section class="content-row-vacature main-padding">
    <div class="vacature-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
        <div class="vacature-text">
            <h4 class="h4-quote">@field('vacature_subtitel')</h4>
            <h1>@field('vacature_titel')</h1>
            <div class="v-info">
                <div><img src="@asset('images/map.svg')" alt="map icon"><p>@field('vacature_locatie')</p></div>
                <div><img src="@asset('images/clock.svg')" alt="clock icon"><p>@field('vacature_tijd')</p></div>
                <div><img src="@asset('images/school.svg')" alt="school icon"><p>@field('vacature_opleiding')</p></div>
            </div>
            <div>@field('vacature_editor')</div>    
        </div>
        <div class="vacature-list">
            <ul class="list">
                @fields('vacature_usp')
                    <li>@sub('vacature_usp_item')</li>
                @endfields
            </ul>
        </div>
        <div class="double-border">
            <span class="border-1"></span>
            <span class="border-2"><span>
        </div>
    </div>
</section>

