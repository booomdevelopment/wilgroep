<section class="contact-form-container">
    <div class="width-inner contact-form-wrapper">
        <div class="form-wrapper">
            <?php echo do_shortcode( '[contact-form-7 id="271" title="Contact formulier"]' ); ?>
        </div>
    </div>
</section>