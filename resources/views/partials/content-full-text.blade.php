<section class="intro-row main-padding">
    <div class="intro-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
        <div class="intro-text">
            @group('intro_tekst_blok')
                <h4 class="h4-quote">@sub('intro_quote')</h4>
                <h2 class="h2-title">@sub('intro_titel')</h2>
                <p>@sub('intro_tekst')</p>
            @endgroup
        </div>
        <div class="double-border">
            <span class="border-1"></span>
            <span class="border-2"><span>
        </div>
    </div>
</section>