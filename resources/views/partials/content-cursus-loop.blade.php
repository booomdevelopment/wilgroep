<section class="c__cursus-loop">
    <div class="w__cursus-loop width-inner">
        @if (!is_archive('cursussen'))
            
            @if (is_archive())
                <h2 class="h2-title">Leerwerktrajecten</h2>    
            @else
                <h2 class="h2-title"><?php the_field('cursussen_titel') ?></h2>  
            @endif
        @endif
        <div class="loop__wrapper flex-it f-row f-wrap f-just-start">
            <?php
            $args = array(  
                'post_type' => 'cursussen',
                'post_status' => 'publish',
                'posts_per_page' => 6, 
                'orderby' => 'title', 
                'order' => 'ASC',
            );
    
            $loop = new WP_Query( $args ); 
                
            while ( $loop->have_posts() ) : $loop->the_post(); 
            ?>
                <div class="cursus__wrapper">
                    <h3 class="single__title">@title</h3>
                    <div class="single__excerpt">
                        <?= wp_trim_words( get_the_content(), 20, '...' ); ?>
                    </div>
                    <a class="main-button-vac" href="@permalink">Lees meer</a>
                    
                </div>
            <?php
            endwhile;
    
            wp_reset_postdata(); 
            ?>
        </div>
    </div>
</section>

