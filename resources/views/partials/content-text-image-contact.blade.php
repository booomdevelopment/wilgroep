<section class="about-row-image main-padding">
    <div class="about-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
    <div class="about-text">
        @group('links_tekst_blok_contact')
            <h4 class="h4-quote">@sub('links_quote_contact')</h4>
            <h2 class="h2-title">@sub('links_titel_contact')</h2>
            @sub('links_tekst_contact')
        @endgroup
    </div>
    <div class="about-image">
        <img src='@field("rechts_foto_blok_contact", "url")' alt='@field("rechts_foto_blok_contact", "url")' />
        <div class="contact-image-overlay">
            <div class="contact-image-overlay-text">@field("rechts_foto_blok_hover_text")</div>
        </div>
    </div>
    <div class="double-border">
        <span class="border-1"></span>
        <span class="border-2"><span>
    </div>
    </div>
</section>