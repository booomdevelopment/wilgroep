<section class="content-row-vacature main-padding">
    <div class="vacature-row-wrapper width-inner flex-it f-row f-just-between f-align-center">
        <div class="vacature-text">
            <h1>@title</h1>

            <div>@content</div>    
        </div>

        <div class="double-border">
            <span class="border-1"></span>
            <span class="border-2"><span>
        </div>
    </div>
</section>

