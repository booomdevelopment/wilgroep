{{--
  Template Name: About us template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-small-header')
    @include('partials.content-text-button-image')
    @include('partials.content-text-double')
    @include('partials.content-ups-banner')
    @include('partials.content-cta-banner')
  @endwhile
@endsection
